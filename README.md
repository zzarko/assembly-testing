# Assembly-Testing

skript za automatsko testiranje provera za Arhitekturu računara, verzija 2019-04-24

Za pregledanje zadataka, skriptu je neophodna struktura direktorijuma kakva je dobijena u arhivi:

- zadatak/      - ovde se raspakuje studentsko rešenje
- zadatak-old/  - zadatak prethodnog studenta (za poređenje rešenja)
- zadatak-unm/  - nemodifikovano studentsko rešenje (za kreiranje patch-a)
- tekst/        - tekst zadatka
- arhive/       - raspakovana arhiva pokupljena sa nastavnih materijala
-                 (fajlovi sa nazivima provera_YYYY-MM-DD_HH-MM_sXXX.tgz)
- x             - ovaj skript
- tNN           - fajlovi sa testovima
- resenje.S     - tačno rešenje zadatka
- glavni.c      - glavni program za zadatke sa potprogramom
- template.sh   - fajl za generisanje testiraj.sh skripta
- runzad        - expect skript
- kill.sh       - skript za čekanje za završetak rada

Za generisanje skriptova za testiranje (./x -t) su potrebni sledeći fajlovi:

- x             - ovaj skript
- tNN           - fajlovi sa testovima
- resenje.S     - tačno rešenje zadatka
- glavni.c      - glavni program za zadatke sa potprogramom
- template.sh   - fajl za generisanje testiraj.sh skripta
- runzad        - expect skript
- kill.sh       - skript za čekanje za završetak rada

Skriptu je neophodan fajl sistem koji podržava RWX prava
(ne radi na NTFS/FAT fajl sistemima!)

Skriptu su neophodni sledeći programi za rad: bash, grep, diff, sed, expect,
fldiff (ili neki drugi vizuelni diff), gedit (ili neki drugi tekst editor)



## STRUKTURA TEST FAJLOVA

Test fajlovi sadrže linije koje služe za opis ulaza i izlaza programa. Njihovi nazivi treba
da budu u stilu tNN gde je NN dvocifreni redni broj testa.

### Ulazne linije za opis tekstualnog unosa (read sistemski poziv):

      @neki tekst
      @neki drugi tekst
      @

Može ih biti više, odnosno onoliko koliko različitih ulaza treba uneti za jedno pokretanje programa.
Poslednja linija predstavlja primer unosa praznog stringa (samo Enter). Zbog trenutnog ograničenja
generisanja testiraj.sh fajla, ne treba koristiti ulaz koji se sastoji samo od znaka '#' (@#), koji
se tamo upotrebljava za oznaku prazne linije (biće popravljeno za narednu godinu).

### Ulazne linije za opis unosa preko promenljivih:

      a = 18
      niz: .long 1,2,3,4,5
      b: .byte 0

Promenljive i konstante se definišu isto kao i u asemblerskom programu. Da bi ovo radilo, studenti
moraju zadržati nazive promenljivih onako kako su zadate u inicijalnom zad.S fajlu. Ako to nije slučaj,
studentski zadatak se mora ili prepraviti da koristi takve nazive ili pregledati ručno.

### Izlazne linije za opis tekstualnog izlaza:

      #izlaz3
      #str1
      #

Da bi automatsko prepoznavanje tekstualnog izlaza radilo, mora postojati promenljiva sa inicijalnim
tekstom za izlaz, npr:

    izlaz3: .ascii "Rezultat je:"

iza koje treba da bude ispisan rezultat rada programa. Sam tekst te promenljive je nebitan, bitno je
da se nakon ispisa teksta iz promenljive koja se tako zove na ekranu ispisuje rezultat rada.
Skript će onda porediti linije studentskog izlaza koje počinju tim tekstom sa linijama tačnog izlaza.
Takvih linija može biti više, odnosno može se proveravati više izlaza ako ih program ima. Da bi ovo
radilo, studenti moraju zadržati nazive promenljivih onako kako su zadate u inicijalnom zad.S fajlu.
Ako to nije slučaj, studentski zadatak se mora ili prepraviti da koristi takve nazive ili pregledati ručno.
Može se porediti i kompletan izlaz programa ukoliko se kao provera izlaza unese samo taraba i tada to mora
biti jedina linija za opis tekstualnog izlaza

### Izlazne linije za opis izlaza preko promenljivih:

    #R 4
    #niz 8@10
    #Greska 1g

U svakoj linije za opis promenljive treba da stoji njen naziv i opis za prikaz i proveru. Opis provere
i ispisa može biti jedan od sledećih:

    1, 2, 4 ili 8     - kao 1,2,4 ili 8-bajtna promenljiva, dekadni sistem, označeno
    1u, 2u, 4u ili 8u - kao 1,2,4 ili 8-bajtna promenljiva, dekadni sistem, neoznačeno
    1x, 2x, 4x ili 8x - kao 1,2,4 ili 8-bajtna promenljiva, heksadecimalni sistem
    1g, 2g, 4g ili 8g - kao 1,2,4 ili 8-bajtna promenljiva, dekadni sistem, označeno, promenljiva za grešku

Opis provere može na kraju imati dodatak "@X", gde je X broj elemenata niza koji se proverava.
Ako je neka promenljiva označena kao promenljiva za grešku, to utiče na proveru tačnosti rešenja:

- ako je greška = 0, ostale promenljive moraju da se poklapaju sa tačnim rešenjem
- ako je greška != 0, vrednosti ostalih promenljivih se ne posmatraju

Provere promenljivih se mogu kombinovati sa proverama tekstualnog izlaza.

### Test fajl može da sadrži i liniju sa težinskim faktorima, npr: (zasad se ne koristi)

    //TEZINA 15
    //TEZINA -10
    //TEZINA 15 -5
    //TEZINA -15 30

Pozitivna vrednost će se dodavati ako je test prošao, negativna ako nije. Ukoliko je
neka od vrednosti, pozitivna ili negativna, nije navedena, podrazumeva se da je 0.
Na kraju se ispisuje i procenat ovakvih težinskih poena, ako su zadati (ukupna suma se
računa tokom prolaska kroz testove, pri čemu se sabiraju samo pozitivne vrednosti)
Ako se zada i broj poena za test (-p), ispisaće se i koliko bi poena bilo osvojeno.

### Testiranje potprograma

Za testiranje potprograma treba napraviti fajl "glavni.c" u kome će se nalaziti glavni program za pozivanje
potprograma. Glavni program treba da bude napravljen tako da je ulaz sa tastature, a izlaz na ekran (pogledati
primere). Glavni program takođe treba da sadrži deklaraciju promenljive RUNPP_REG_ERR, kao i return liniju kako
je data u template fajlu za glavni.c. Ovaj glavni program se daje i studentima prilikom testa.

Ukoliko potprogram vraća vrednost greške, nju treba postaviti u promenljivu 'g' i ta vrednost se vraća
kao izlazni kod glavnog programa.

Pored fajla 'glavni.c', u template direktorijumu je dat i 'funkcije.c' u kome se nalaze razne funkcije za ispis
koje su se pokazale kao zgodne prethodnih godina, te se mogu iskoristiti za pravljenje ispisa.

Test fajlovi se prave tako da se opisuju ulazne linije, a kao izlaz se posmatra kompletan izlaz (#). Za ovako
pripremljene testove se može napraviti testiraj.sh koji se može dati studentima za samostalno testiranje.


# PODGRUPE

Skript podržava rad sa dve podgrupe, 'a' i 'b'. Nazivi test fajlova i direktorijuma za rešenje
za podgrupu 'a' su isti kao kada se podgrupe ne koriste. Ako se podgrupe koriste, treba napraviti
i fajl resenje2.S i u njega staviti rešenje zadatka za podgrupu 'b'. Nazivi test fajlova
za podgrupu 'b' treba da imaju oblik 'tbNN' umesto sa 'tNN' i treba da se nalaze u osnovnom
direktorijumu, gde su i test fajlovi za podgrupu 'a'.

Da bi skript mogao da automatski odredi podgrupu, treba postaviti promenljivu PODGRUPA (prva na
spisku promenljivih podešavanja) na string:

    PODGRUPA='<podgrupa> <fajl> <kljucna_reč>'

gde je <podgrupa> slovo 'a' ili 'b', a <fajl> je fajl u kome će se tražiti <kljucna_reč> (može
biti i regularan izraz po 'grep -E' sintaksi). Na primer:

    PODGRUPA='a zad.S izlaz2:'

će definisati da će podgrupa biti 'a' ukoliko se u fajlu 'zad.S' nalazi tekst 'izlaz2:', a u suprotnom
će podgrupa biti 'b'. Ukratko, treba uočiti nešto što sigurno razlikuje zadatke za podgrupe. Takođe,
zadatke za podgrupe treba zadati tako da budu dovoljno različiti, da se može odraditi automatsko
razlikovanje podgrupa.



# PRIMERI UPOTREBE

    ./x -h

Prikazuje sve opcije koje skript podržava (neke opcije isključuju upotrebu pojedinih drugih opcja)

    ./x -d 05

Raspakuje arhivu sa radnog mesta 05, pokreće testove i prikazuje vizuelni diff

    ./x

Ponovo pokreće testove sa tekućim studentom (npr, nakon izmene koda), bez prikaza vizuelnog diff-a

    ./x -r "t14 t15"

Ponovo pokreće testove sa tekućim studentom, ali samo t14 i t15

    ./x -g b 06

Zadaje da je podgrupa 'b', raspakuje arhivu sa radnog mesta 05 i pokreće testove

    ./x -n -d

Raspakuje arhivu sa sledećeg radnog mesta (npr. ako je tekuće bilo 05, onda raspakuje 06),
pokreće testove i prikazuje vizuelni diff

    ./x -s

Ukoliko su prilikom pregleda zadatka napravljene izmene na studentskom rešenju kako bi se
ono popravilo ili videlo da li nakon uklanjanja neke greške ostatak programa radi, te izmene
se mogu sačuvati u 'arhive/XX.patch' fajlu radi kasnije prezentacije studentu kada dođe na
uvid u rad (da se pokaže kako je rešenje trebalo da izgleda) ili čisto za evidenciju...

    ./x -a 07

Raspakuje arhivu sa radnog mesta 07 i primenjuje na njega zapamćene izmene (arhive/07.patch)
Nakon primene izmena, otvara vizuelni diff alat za sve izmenjene fajlove

    ./x -t

Pokreće generisanje testiraj.sh fajla za sve test fajlove, a na osnovu izlaza tačnog rešenja.
Ukoliko postoje podgrupe, generisaće testiraj.sh za podgrupu 'a'. Za podgrupu 'b' treba
upotrebiti opciju '-g b'. Ukoliko ne treba staviti sve test fajlove, treba uoptrebiti opciju -r

    ./x -p 20   (zasad se ne koristi)

Zadavanje broja poena na testu. Ukoliko test fajlovi imaju težinske faktore u sebi, nakon
odrađenih testova će se ispisati koliko poena je student dobio od maksimalnih 20. Ukoliko
težinskih faktora nema, računaće se da svaki test nosi 1 poen.

    ./csv 20    (zasad se ne koristi)

Pored 'x' skripta, u direktorijumu se nalazi i 'csv' skript koji pokreće testiranje za sva
radna mesta i generiše fajl sa rezultatima koji se direktno može uvesti u evidenciju.
Jedini parametar koji se mora navesti je maksimalan broj poena koji test nosi. Korišćenje
ove opcije bi trebalo da bude praćeno i težinskim faktorima u svim testovima. U principu je
napravljeno za neku eventualnu kasniju upotrebu, zasad eksperimentalno.




## Istorija poslednjih izmena

2019-04-23

* dodato definisanje makroa TESTIRAJ kada se se generiše testiraj.sh fajl za potprograme
* može se koristiti za različit rad lavnog programa ako se radi automatsko testiranje, na primer za isključivanje kolor ispisa (trenutno nije podržan od strane testiraj.sh)

2019-04-03

* preuzimanje nizova u dvostrukoj preciznosti za velike vrednosti elemenata nije radilo kako treba, sad radi
* opcija za raspakivanje patch-a **-a** sada radi i bez argumenta (tada raspakuje patch za tekuće radno mesto)
* opcija za eksplicitno zadavanje grupe **-g** sada pamti izbor u fajlu arhive/NN.group i svako sledeće raspakivanje će koristiti zapamćenu grupu (kada automatsko prepoznavanje grupe zakaže iz bilo kog razloga, na ovaj način se može zapamtiti korektna grupa)
* skript će na početku proveriti da li u direktorijumu postoji **xsettings** fajl, i ako postoji, uzeti podešavanja koja su u njemu umesto onih koja stoje na početku fajla. Nadam se da će ovo olakšati upgrade na novu verziju, da ne morate stalno menjati programe na one koji vam više odgovaraju (a i da ja ne moram menjati kada nešto ispravljam)

2019-03-27

* ova verzija je pogrešno označena sa 2019-04-27, trebalo je biti 2019-03-27
* dodat underscore kao validan znak za ime potprograma
* nakon raspakivanja svim fajlovima se dodaju RW prava
* dodato **export NO_AT_BRIDGE=1** radi izbegavanja ispisivanja raznih upozorenja GTK programa

